Welcome!
========

This library contains the components required to integrate an
OpenStack deployment with a Big Switch Networks fabric.

* Source: https://opendev.org/x/networking-bigswitch
* Bugs: https://bugs.launchpad.net/networking-bigswitch


External Resources:
===================

Big Switch Networks Website: http://www.bigswitch.com
