# Copyright 2017, Big Switch Networks
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

BSN_SERVICE_PLUGIN = 'BSNSERVICEPLUGIN'

# state_syncer constants
NOVA_CLIENT_VERSION = '2'
INTF_MAC_ADDR = 'OS-EXT-IPS-MAC:mac_addr'
INTF_IP_ADDR = 'addr'
INTF_IP_TYPE = 'OS-EXT-IPS:type'
INTF_IP_VERSION = 'version'
HYPERVISOR_HOSTNAME = 'OS-EXT-SRV-ATTR:hypervisor_hostname'
VM_STATE = 'OS-EXT-STS:vm_state'

# os-net-config constants
OVS_AGENT_INI_FILEPATH = '/etc/neutron/plugins/ml2/openvswitch_agent.ini'
RH_NET_CONF_PATH = "/etc/os-net-config/config.json"
